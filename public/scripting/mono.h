
#ifndef RIAD_SCRIPTING_MONO_H_
#define RIAD_SCRIPTING_MONO_H_

#include <base/scripting.h>
#include <memory>

namespace Scripting
{
class Mono : public Base::Scripting::Interface
{
private:
    class Implementation;
private:
    std::unique_ptr<Implementation> m_impl;
public:
    Mono();
    virtual ~Mono() final;

    virtual bool init() final;

    virtual bool load(const char *path) final;

    virtual int run(int argc, const char *argv[]) final;
};
} // Scripting

#endif // RIAD_SCRIPTING_MONO_H_


#include <scripting/mono.h>
#include <string>
#include <vector>

#include <mono/jit/jit.h>
#include <mono/metadata/assembly.h>

#include "capi.h"

namespace Scripting
{
    
namespace mono
{
// MONO CApi implementation..
void print(MonoString *message)
{
    char *str = mono_string_to_utf8 (message);
    riad_print(str);
    mono_free(str);
}

bool bound = false;
void bind()
{
    if(bound) return;
    bound = true;
    
    mono_add_internal_call("Scripting.Riad::print", (const void *)print);
}
} // mono
    
class Mono::Implementation
{
private:
    std::string name = "RIAD Scripting";
    MonoDomain *domain = nullptr;
    MonoAssembly *assembly = nullptr;
public:
    ~Implementation()
    {
        destroy();
    }
    
    bool init()
    {
        domain = mono_jit_init(name.c_str());
        mono::bind();
        return domain != nullptr;
    }

    bool load(const std::string& path)
    {
        /// For now only supporting single assembly
        if(assembly != nullptr)
        {
            return false;
        }
        assembly = mono_domain_assembly_open (domain, path.c_str());
        return assembly != nullptr;
    }

    int run(int argc, const char *argv[])
    {
        std::vector<char*> arr;
        arr.resize(argc);
        for(size_t i = 0 ; i < argc ; ++i)
        {
            size_t len = strlen(argv[i]);
            arr[i] = new char[len + 1];
            memcpy(arr[i], argv[i], len);
            arr[i][len] = '\0';
        }
        int ret = mono_jit_exec(domain, assembly, argc, arr.data());
        for(auto& carr : arr)
        {
            delete[] carr;
        }
        return ret;
    }

    void destroy()
    {
        if(domain != nullptr)
        {
            mono_jit_cleanup (domain);
        }
        domain = nullptr;
        assembly = nullptr;
    }
};

Mono::Mono()
: m_impl(new Implementation)
{
}

Mono::~Mono()
{
}

bool Mono::init()
{
    return m_impl->init();
}

bool Mono::load(const char *path)
{
    return m_impl->load(path);
}

int Mono::run(int argc, const char *argv[])
{
    return m_impl->run(argc, argv);
}

} // ns Scripting

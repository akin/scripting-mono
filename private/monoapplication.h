
#ifdef EXAMPLE
#ifndef MONOTEST_H_
#define MONOTEST_H_

#include <base/application.h>

class MonoApplication : public Base::Application
{
public:
    MonoApplication();
    virtual ~MonoApplication();

    virtual bool init() override;

    virtual int run() override;
};

#endif
#endif